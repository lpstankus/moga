# Keyboard names
$keyboards=monsgeek-m1-keyboard,logitech-pro-k/da

# Monitor names
$m0=DP-1
$m1=HDMI-A-1

# Monitor configs
monitor = $m0, 3840x2160@144,  0x0, 1.5
monitor = $m1, 1920x1080@240, -1920x0, 1.0

# Startup apps
exec-once = dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
exec-once = hyprpaper & launch_waybar
exec-once = steam & spotify & discord

# Default programs
$menu = wofi --show drun
$terminal = kitty
$browser = firefox-developer-edition
$fileManager = nautilus

# Env vars
env = XCURSOR_SIZE,24

env = MOZ_ENABLE_WAYLAND=1

env = GDK_BACKEND=wayland,x11
env = SDL_VIDEODRIVER=wayland
env = CLUTTER_BACKEND=wayland

env = XDG_SESSION_TYPE=wayland
env = XDG_CURRENT_DESKTOP=Hyprland
env = XDG_SESSION_DESKTOP=Hyprland

env = QT_AUTO_SCREEN_SCALE_FACTOR=1
env = QT_QPA_PLATFORM=wayland;xcb
env = QT_WAYLAND_DISABLE_WINDOWDECORATION=1
env = QT_QPA_PLATFORMTHEME=qt5ct

input {
    kb_layout = us,us
    kb_variant = ,intl

    repeat_rate = 60
    repeat_delay = 200

    follow_mouse = 1
    sensitivity = -0.3
    accel_profile = flat

    touchpad {
        disable_while_typing = true
        natural_scroll = true
    }
}

general {
    gaps_in = 5
    gaps_out = 5
    border_size = 2
    col.active_border = rgb(adc38d) rgb(bed6e1)
    col.inactive_border = rgba(595959aa)

    layout = master
    allow_tearing = false
}

decoration {
    rounding = 5

    blur {
        enabled = true
        size = 5
        passes = 2
    }

    layerrule = blur, waybar

    shadow_range = 30
    shadow_render_power = 3
    col.shadow = rgba(1a1a1abb)
    drop_shadow = yes
}

animations {
    enabled = yes

    bezier = lin, 0.5, 0.5, 0.5, 0.5
    bezier = qck, 0.1, 0.9, 0.1, 0.9
    bezier = ovr, 0.0, 1.1, 0.5, 1.0
    bezier = qckovr, 0.0, 1.2, 0.5, 1.0
    # bezier = window_default, 0.05, 0.9, 0.1, 1.05

    animation = windows,     1, 3, qckovr
    animation = windowsIn,   1, 3, qckovr, slide
    animation = windowsOut,  1, 5, qckovr, slide

    animation = border, 1, 10, qck
    animation = borderangle, 1, 120, lin, loop

    animation = fade,    1, 7, qck
    animation = fadeIn,  1, 7, qck
    animation = fadeOut, 1, 7, qck
    # animation = fadeDim, 0, 7, qck
    # animation = fadeSwitch, 1, 7, qck

    animation = workspaces, 1, 7, ovr, slidefade
}

master {
    mfact = 0.5
    no_gaps_when_only = 0
    drop_at_cursor = true
}

gestures {
    workspace_swipe = on
    workspace_swipe_fingers = 3
}

misc {
    disable_hyprland_logo = true
    disable_splash_rendering = true
    # splash = false

    enable_swallow = false
    swallow_regex = ^(Alacritty|kitty|footclient)$
}

xwayland {
    use_nearest_neighbor = false
}

# Fix workspaces to their respective monitors
workspace = 01, monitor:$m0, default:true
workspace = 02, monitor:$m0
workspace = 03, monitor:$m0
workspace = 04, monitor:$m0
workspace = 05, monitor:$m0
workspace = 06, monitor:$m0
workspace = 08, monitor:$m0
workspace = 07, monitor:$m0
workspace = 09, monitor:$m0
workspace = 10, monitor:$m0
workspace = 11, monitor:$m1, default:true
workspace = 12, monitor:$m1
workspace = 13, monitor:$m1
workspace = 14, monitor:$m1
workspace = 15, monitor:$m1
workspace = 16, monitor:$m1
workspace = 17, monitor:$m1
workspace = 18, monitor:$m1
workspace = 19, monitor:$m1
workspace = 20, monitor:$m1

# Workspace motion (current monitor)
bind = SUPER, 1, exec, hypr_workspace 1
bind = SUPER, 2, exec, hypr_workspace 2
bind = SUPER, 3, exec, hypr_workspace 3
bind = SUPER, 4, exec, hypr_workspace 4
bind = SUPER, 5, exec, hypr_workspace 5
bind = SUPER, 6, exec, hypr_workspace 6
bind = SUPER, 7, exec, hypr_workspace 7
bind = SUPER, 8, exec, hypr_workspace 8
bind = SUPER, 9, exec, hypr_workspace 9
bind = SUPER, 0, exec, hypr_workspace 0
bind = SUPER SHIFT, 1, exec, hypr_workspace --move 1
bind = SUPER SHIFT, 2, exec, hypr_workspace --move 2
bind = SUPER SHIFT, 3, exec, hypr_workspace --move 3
bind = SUPER SHIFT, 4, exec, hypr_workspace --move 4
bind = SUPER SHIFT, 5, exec, hypr_workspace --move 5
bind = SUPER SHIFT, 6, exec, hypr_workspace --move 6
bind = SUPER SHIFT, 7, exec, hypr_workspace --move 7
bind = SUPER SHIFT, 8, exec, hypr_workspace --move 8
bind = SUPER SHIFT, 9, exec, hypr_workspace --move 9
bind = SUPER SHIFT, 0, exec, hypr_workspace --move 0

# Workspace motion (main monitor)
bind = SUPER CONTROL_L, 1, workspace, 01
bind = SUPER CONTROL_L, 2, workspace, 02
bind = SUPER CONTROL_L, 3, workspace, 03
bind = SUPER CONTROL_L, 4, workspace, 04
bind = SUPER CONTROL_L, 5, workspace, 05
bind = SUPER CONTROL_L, 6, workspace, 06
bind = SUPER CONTROL_L, 7, workspace, 07
bind = SUPER CONTROL_L, 8, workspace, 08
bind = SUPER CONTROL_L, 9, workspace, 09
bind = SUPER CONTROL_L, 0, workspace, 10

# Workspace motion (relative)
bind = SUPER, l, workspace, r+1
bind = SUPER, h, workspace, r-1
bind = SUPER SHIFT, l, movetoworkspace, r+1
bind = SUPER SHIFT, h, movetoworkspace, r-1

# Monitor motion
bind = SUPER CONTROL_L, l, focusmonitor, +1
bind = SUPER CONTROL_L, h, focusmonitor, -1
bind = SUPER CONTROL_L SHIFT, l, movewindow, mon:+1
bind = SUPER CONTROL_L SHIFT, h, movewindow, mon:-1

# Layout motion
bind = SUPER, U, layoutmsg, focusmaster
bind = SUPER, I, layoutmsg, swapwithmaster
bind = SUPER, K, layoutmsg, cycleprev
bind = SUPER, J, layoutmsg, cyclenext
bind = SUPER SHIFT, K, layoutmsg, swapprev
bind = SUPER SHIFT, J, layoutmsg, swapnext

bind = ALT_L, TAB, cyclenext
bind = ALT_L SHIFT_L, TAB, cyclenext, prev

# Resize motion
bind = SUPER CONTROL_L, K, resizeactive,  5% 0%
bind = SUPER CONTROL_L, J, resizeactive, -5% 0%

# Window control
bind = SUPER, Q, exec, hypr_closewindow
bind = SUPER, S, togglefloating,
bind = SUPER, M, fullscreen, 1
bind = SUPER SHIFT, M, fullscreen, 0

# Launch standard programs
bind = SUPER, RETURN, exec, $terminal
bind = SUPER,      F, exec, $fileManager
bind = SUPER,      B, exec, $browser
bind = SUPER,      P, exec, $menu

# Mouse controls
bind =  SUPER, mouse_down, workspace, r+1
bind =  SUPER,   mouse_up, workspace, r-1
bind =  SUPER,  mouse:274, killactive
bindm = SUPER,  mouse:272, movewindow
bindm = SUPER,  mouse:273, resizewindow

# Multimedia control (play/pause, stop, prev, next)
bind = , XF86AudioPlay, exec, playerctl play-pause
bind = , XF86AudioStop, exec, playerctl stop
bind = , XF86AudioPrev, exec, playerctl previous
bind = , XF86AudioNext, exec, playerctl next

# Volume control
bind = , XF86AudioMute,        exec, pactl set-sink-mute 0 toggle
bind = , XF86AudioRaiseVolume, exec, pactl -- set-sink-volume 0 +5%
bind = , XF86AudioLowerVolume, exec, pactl -- set-sink-volume 0 -5%

# Keyboard layout
bind = SUPER, SPACE, exec, hypr_cyclekeyboard $keyboards

# Exit
bind = SUPER SHIFT, E, exit

# Window rules
windowrule = opacity 0.9,^(Spotify)$

windowrule = workspace 11 silent,^(Spotify)$
windowrule = workspace 10 silent,^(steam)$
windowrule = workspace 12 silent,^.*(discord).*$

windowrulev2 = suppressevent maximize, class:.*

windowrulev2 = float,class:^(firefox)$,title:^$
windowrulev2 = noinitialfocus,class:^(firefox)$,title:^$

windowrulev2 = opacity 0.0 override 0.0 override,class:^(xwaylandvideobridge)$
windowrulev2 = noanim,class:^(xwaylandvideobridge)$
windowrulev2 = nofocus,class:^(xwaylandvideobridge)$
windowrulev2 = noinitialfocus,class:^(xwaylandvideobridge)$
