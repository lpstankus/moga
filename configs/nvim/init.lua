--------------------------------------------------------------------------------
-----┌────────────────────────────────────────────────────────────────────┐-----
-----│                    _   __         _    ___                         │-----
-----│                   / | / /__  ____| |  / (_)___ ___                 │-----
-----│                  /  |/ / _ \/ __ \ | / / / __ `__ \                │-----
-----│                 / /|  /  __/ /_/ / |/ / / / / / / /                │-----
-----│                /_/ |_/\___/\____/|___/_/_/ /_/ /_/                 │-----
-----│                                                                    │-----
-----└────────────────────────────────────────────────────────────────────┘-----
----- General ------------------------------------------------------------------

---------- Init ----------------------------------------------------------------

require('lazypath_config')

local ag = vim.api.nvim_create_augroup("MOGA", { clear = true })

local map = function(mode, key, cmd)
  vim.keymap.set(mode, key, cmd, { noremap = true })
end

local au_fn = function(evt, pat, fn)
  local wrap_fn = function() fn() end
  vim.api.nvim_create_autocmd(evt, { pattern = pat, callback = wrap_fn, group = ag })
end

local au_lua = function(evt, pat, fn)
  vim.api.nvim_create_autocmd(evt, { pattern = pat, callback = fn, group = ag })
end

local au_vim = function(evt, pat, cmd)
  vim.api.nvim_create_autocmd(evt, { pattern = pat, command = cmd, group = ag })
end

---------- Sets ----------------------------------------------------------------

vim.g.markdown_recomended_style = 0
vim.o.termguicolors = true
vim.o.mouse  = "a"
vim.o.wrap   = false
vim.o.hidden = true

vim.o.expandtab   = true
vim.o.tabstop     = 4
vim.o.shiftwidth  = 4
vim.o.softtabstop = 4

vim.o.showmode   = false
vim.o.shortmess  = "filaoOsTIcF"
vim.o.laststatus = 3

vim.o.updatetime  = 50
vim.o.completeopt = "menuone,preview,noselect"

vim.o.scrolloff  = 10
vim.o.cursorline = true
vim.o.signcolumn = "yes"
vim.o.number     = true
vim.o.relativenumber = true

vim.o.ignorecase = true
vim.o.smartcase  = true

vim.o.incsearch  = true
vim.o.inccommand = "split"

vim.o.splitbelow = true
vim.o.splitright = true
vim.o.splitkeep = "screen"

vim.o.undofile = true
vim.cmd [[ set undodir=~/.cache/nvim/undodir ]]

vim.o.list      = true
vim.o.listchars = "tab:> ,extends:›,precedes:‹,nbsp:␣,trail:·"

vim.o.foldlevel    = 99
vim.o.foldmethod   = "indent"
vim.o.fillchars    = "fold: "
vim.o.foldnestmax  = 10
vim.o.foldminlines = 1

-- Neovide
if vim.g.neovide then
  vim.o.guifont = "iosevkatermslab nerd font:h11"
  vim.opt.linespace = 0

  vim.g.neovide_fullscreen = false
  vim.g.neovide_hide_mouse_when_typing = true

  vim.g.neovide_scroll_animation_far_lines  = 1
  vim.g.neovide_scroll_animation_length     = 0.25
  vim.g.neovide_cursor_trail_size           = 0.8
  vim.g.neovide_cursor_animation_length     = 0.04
  vim.g.neovide_cursor_animate_command_line = true
  vim.g.neovide_cursor_antialiasing = true
  vim.g.neovide_cursor_vfx_mode     = "wireframe"
end

-- Colorscheme
vim.cmd.colorscheme("soneca")

---------- Vanilla Keymaps -----------------------------------------------------

vim.g.mapleader      = " "
vim.g.maplocalleader = " "

map("i", "<c-c>", "<esc>")
map("n", "<leader>r", ":%s/")
map("v", "<leader>o", "\"_dp")
map({ "n", "v" }, "<leader>f", "za")

-- List navigation
map("n", "<leader>j", "<cmd>cnext<cr>zz")
map("n", "<leader>k", "<cmd>cprev<cr>zz")
map("n", "<leader>J", "<cmd>lnext<cr>zz")
map("n", "<leader>K", "<cmd>lprev<cr>zz")

-- Centering stuff
map("n", "n", "nzz")
map("n", "N", "Nzz")
map("n", "J", "mzJ`z")
map("n", "<c-d>", "<c-d>zz")
map("n", "<c-u>", "<c-u>zz")

-- Undo breakpoints
for _, char in pairs({ "_", "\"", "'", ",", ".", ";", "!", "?", "(", ")", "[", "]", "{", "}" })  do
  map("i", char, char .. "<c-g>u")
end

-- Jumplist mutations
vim.cmd [[
  nnoremap <expr> k (v:count > 5 ? "m'" . v:count : "") . 'k'
  nnoremap <expr> j (v:count > 5 ? "m'" . v:count : "") . 'j'
]]

-- Move lines
map("v", "J", "<cmd>m '>+1<cr>gv=gv")
map("v", "K", "<cmd>m '<-2<cr>gv=gv")

-- System clipboard actions
map("n", "<leader>y", "\"+y" )
map("n", "<leader>Y", "\"+y$")
map("n", "<leader>p", "\"+p" )
map("n", "<leader>P", "\"+P" )
map("v", "<leader>y", "\"+y" )
map("v", "<leader>p", "\"+p" )
map("v", "<leader>P", "\"+P" )

-- Tab
map("n", "<leader>tn", "<cmd>tab new<cr>")
map("n", "<leader>tq", "<cmd>tab close<cr>")
map("n", "<leader>tmn", "<cmd>tab move +1<cr>")
map("n", "<leader>tmp", "<cmd>tab move -1<cr>")

-- Indentation
map("n", "<tab>", ">>")
map("n", "<s-tab>", "<<")
map("v", "<tab>", ">")
map("v", "<s-tab>", "<")

-- Source nvim
map("n", "<leader>vs", "<cmd>source ~/.config/nvim/init.lua<cr>")

-- Window shortcuts
map("n", "<leader>w.", "<cmd>Explore<cr>")
map("n", "<leader>ws", "<cmd>Sexplore<cr>")
map("n", "<leader>wv", "<cmd>Vexplore<cr>")

-- Inspect highlight group
map("n", "<leader>.", "<cmd>Inspect<cr>")

-- Diagnostics
map("n", "<leader>ln", vim.diagnostic.goto_next)
map("n", "<leader>lp", vim.diagnostic.goto_prev)
map("n", "<leader>ls", function()
  vim.diagnostic.open_float({
    scope = "line",
    severity_sort = true,
    source = "if_many",
    border = "single"
  })
end)

----- Commands/Auto-commands ---------------------------------------------------

vim.api.nvim_create_user_command("W", "w", { bang = true })
vim.api.nvim_create_user_command(
  "Indent",
  function(args)
    if args.args then
      local len = tonumber(args.args)
      vim.o.tabstop     = len
      vim.o.softtabstop = len
      vim.o.shiftwidth  = len
    end
  end,
  { nargs = 1 }
)

au_fn("TextYankPost", nil, require('vim.highlight').on_yank)
au_vim({ "BufRead", "BufNewFile" }, { "*.h", "*.c" }, "set noexpandtab")
au_vim({ "BufRead", "BufNewFile" }, { "*.h", "*.c" }, "Indent 8")

----- Plugin configs -----------------------------------------------------------

---------- Filetype ------------------------------------------------------------

-- Vimtex
vim.g.tex_flavor  = 'latex'
vim.g.tex_conceal = 'abdmg'
vim.g.vimtex_quickfix_mode  = 0
vim.g.vimtex_syntax_enabled = 0
vim.g.vimtex_view_method = 'general'

-- Zig
vim.g.zig_fmt_autosave = 0

-- Vimwiki
local start_wiki = function()
  vim.o.filetype      = "vimwiki"
  vim.o.concealcursor = "n"
end

-- Filetype autocmds
au_vim("FileType", "tex", "set conceallevel=1")
au_lua("FileType", "tex", function() map("n", "<leader><leader>c", "<cmd>VimtexCompile") end)
au_fn({ "BufRead", "BufNewFile" }, "*.wiki", start_wiki)
au_vim({ "BufRead", "BufNewFile" }, "*.wgsl", "set ft=wgsl")
au_vim({ "BufRead", "BufNewFile" }, { "*.vs", "*.fs", "*.comp" }, "set ft=glsl")

---------- Visual flair --------------------------------------------------------

-- require("noice_config")
require("lualine_config")
require("todo-comments").setup { signs = false }
require("smartcolumn").setup {
  colorcolumn = "100",
  disabled_filetypes = {},
  custom_colorcolumn = {
    c = "80",
    gitcommit = "72",
    gitsnedemail = "75",
    ruby = "120",
    vimwiki = "72",
  },
}
require("ibl").setup {
  indent = { char = '▎', tab_char = '▎' },
  scope = {
    show_start = true,
    show_end = false,
    exclude = { language = { "tex" } },
  },
}
require("nvim-highlight-colors").setup {
	render = "virtual",
	virtual_symbol = "■",
	virtual_symbol_prefix = " ",
	virtual_symbol_suffix = "",
	virtual_symbol_position = "eow",
	enable_hex = true,
	enable_rgb = true,
	enable_hsl = true,
	enable_var_usage = true,
	enable_named_colors = false,
	enable_tailwind = true,
}

---------- Command and autocommands --------------------------------------------

require('guess-indent').setup {
  auto_cmd = true,
  override_editorconfig = false,
  filetype_exclude = {
    "netrw",
    "tutor",
  },
  buftype_exclude = {
    "help",
    "nofile",
    "terminal",
    "prompt",
  },
}

-- Rooter
vim.g.rooter_patterns = { ".git" }

---------- Extensions ----------------------------------------------------------

-- Undotree
map("n", "<leader>uf", "<cmd>UndotreeFocus<cr>")
map("n", "<leader>ut", "<cmd>UndotreeToggle<cr>")

-- Pencil
vim.g["pencil#joinspaces"] = true
vim.g["pencil#autoformat"] = true
vim.g["pencil#wrapModeDefault"] = "soft"
au_vim("FileType", { "markdown", "mkd", "text", "tex" }, "call pencil#init()")

-- qck term
map("n", "<leader>qn", require('qck').new)
map("n", "<leader>qk", require('qck').kill)
map({ "n", "t" }, "<m-q>", require("qck").toggle)

---------- Text manipulation ---------------------------------------------------

require("comment_config")
require("formatter_config")
require("autopairs_config")

-- Easy-Align
vim.cmd [[
  nmap ga <Plug>(EasyAlign)
  xmap ga <Plug>(EasyAlign)
]]

-- Sandwich
vim.cmd [[
  let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)
  let g:sandwich#recipes += [
    \   {'buns': ['{ ', ' }'], 'nesting': 1, 'match_syntax': 1,
    \    'kind': ['add', 'replace'], 'action': ['add'], 'input': ['}']},
    \
    \   {'buns': ['[ ', ' ]'], 'nesting': 1, 'match_syntax': 1,
    \    'kind': ['add', 'replace'], 'action': ['add'], 'input': [']']},
    \
    \   {'buns': ['( ', ' )'], 'nesting': 1, 'match_syntax': 1,
    \    'kind': ['add', 'replace'], 'action': ['add'], 'input': [')']},
    \ ]
]]

-- Iswap
map("n", "<leader>is", "<cmd>ISwap<cr>")

---------- Movement ------------------------------------------------------------

require("harpoon_config")
require("telescope_config")
map("n", "<leader>sf", require("telescope.builtin").find_files)
map("n", "<leader>sb", require("telescope.builtin").buffers)
map("n", "<leader>sl", require("telescope.builtin").current_buffer_fuzzy_find)
map("n", "<leader>sg", require("telescope.builtin").live_grep)
map("n", "<leader>ss", require("telescope.builtin").colorscheme)
map("n", "<leader>st", "<cmd>TodoTelescope<cr>")
map("n", "<leader>sv", function()
  require("telescope.builtin").find_files {
    cwd = "~/.config/nvim",
    find_command = { "rg", "-i", "--hidden", "--files" }
  }
end)

---------- Treesitter ----------------------------------------------------------

require('nvim-treesitter.configs').setup {
  ensure_installed = "all",
  highlight = { enable = true },
  indent = { enable = true },
}

require('treesitter-context').setup {
  enable = true,
  throttle = true,
  max_lines = 5,
  zindex = 20,
  patterns = {
    default = {
      'class',
      'function',
      'method',
      'switch',
    },
    rust = {
      'impl_item',
    },
  },
}

---------- Snippets ------------------------------------------------------------

require("luasnip_config")

-- Scissors
require("scissors").setup {
  editSnippetPopup = { border = "single" },
  telescope = { alsoSearchSnippetBody = true },
  jsonFormatter = "jq",
}

map("n", "<leader><leader>e", require("scissors").editSnippet)
map({ "n", "x" }, "<leader><leader>a", require("scissors").addNewSnippet)

---------- Completion/Lsp/Diagnostics ------------------------------------------

require("bob_config")
require("lsp_config")
require("cmp_config")
require('lsp_signature').setup {
  bind = true,
  hint_enable = false,
  always_trigger = true,
  floating_window_above_cur_line = true,
  handler_opts = { border = "single" },
  hi_parameter = "IncSearch",
}

vim.diagnostic.config({
  underline = true,
  virtual_text = { spacing = 1, prefix = "➤" },
  severity_sort = true,
})

map("n", "<leader>ll", require("telescope.builtin").diagnostics)
map("n", "<leader>lR", require("telescope.builtin").lsp_references)

---------- Vcs -----------------------------------------------------------------

require('gitsigns_config')
require('neogit_config')
