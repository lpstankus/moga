local ls = require('luasnip')
local s, sn = ls.snippet, ls.snippet_node
local f, t, i, c, d = ls.function_node, ls.text_node, ls.insert_node, ls.choice_node, ls.dynamic_node
local rep = require("luasnip.extras").rep
local fmt = require("luasnip.extras.fmt").fmt

return {
  snippets = {
    s(
      "fn",
      fmt(
        [[
          {}fn {}({}) {}{} {{
              {}
          }}
        ]],
        {
          c(4, { t "", t "pub ", t "inline ", t "pub inline " }),
          i(1, "function"),
          i(2, "args"),
          c(5, { t "", t "callconv(.C) ", t "callconv(.Async) ", t "callconv(.Naked) " }),
          i(3, "type"),
          i(0),
        }
      )
    ),
    s(
      "arr",
      fmt("[_{}]{}{{{}}};", { c(1, { t "", sn(nil, { t ":", i(1, "0") }) }), i(2, "type"), i(0) })
    ),
    s(
      "block",
      fmt(
        [[
          {}: {{
              {}
              break :{} val;
          }};
        ]],
        { i(1, "block"), i(0), rep(1) }
      )
    ),
    s(
      "for",
      fmt(
        [[
          {}for ({}) |{}| {{
              {}
          }}
        ]],
        { c(1, { t "", t "inline " }), i(2), i(3), i(0) }
      )
    ),
    s(
      "while",
      fmt(
        [[
          {}while ({}) : ({}) {{
            {}
          }}
        ]],
        { c(1, { t "", t "inline " }), i(2), i(3), i(0) }
      )
    ),
    s(
      "import",
      fmt(
        [[const {} = @import("{}");]],
        {
          f(function(import_name)
            local parts = vim.split(import_name[1][1], '.', true)
            local path = (parts[#parts] == "zig") and parts[#parts - 1] or parts[#parts] or ""
            local path_parts = vim.split(path, '/', true)
            return path_parts[#path_parts] or ""
          end, { 1 }),
          i(1, "std")
        }
      )
    ),
    s("err", fmt([[error.{}]], { i(1, "ErrorName") })),
    s(
      "catch",
      fmt(
        [[
          catch |err|{} {{
              {}
          }};
        ]],
        {
          c(1, { t "", t " switch (err)" }),
          i(0)
        }
      )
    ),
  },
  autosnippets = {},
}

