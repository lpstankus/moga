local ls = require('luasnip')
local s, sn = ls.snippet, ls.snippet_node
local f, t, i, c, d = ls.function_node, ls.text_node, ls.insert_node, ls.choice_node, ls.dynamic_node
local rep = require("luasnip.extras").rep
local fmt = require("luasnip.extras.fmt").fmt

return {
  snippets = {
    s(
      "template",
      fmt(
        [[
          \documentclass{{article}}
          \usepackage[english]{{babel}}
          \usepackage[a4paper,top=2cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{{geometry}}

          \usepackage{{amsmath}}
          \usepackage{{graphicx}}
          \usepackage[colorlinks=true, allcolors=blue]{{hyperref}}

          \title{{{}}}
          \author{{{}}}

          \begin{{document}}
          \maketitle

          {}

          \end{{document}}
        ]],
        { i(1, "title"), i(2, "Lucas Stankus"), i(0) }
      )
    ),
    s(
      "sec",
      fmt(
        [[\{}section{{{}}}{}]],
        {
          c(1, { t "", t "sub", t "subsub" }),
          i(2, "name"),
          c(3, { t "", sn(nil, { t "\\label{", i(1), t "}"}) }),
        }
      )
    ),
    s(
      "begin",
      fmt("\\begin{{{}}}\n\t{}\n\\end{{{}}}", { i(1), i(0), rep(1) })
    ),
    s(
      "frac",
      fmt([[\frac{{{}}}{{{}}}]], { i(1, "upper"), i(2, "lower") })
    ),
    s(
      "sum",
      fmt(
        [[\sum{}]],
        {
          c(
            1,
            {
              t "",
              sn(nil, { t "_{", i(1, "lower"), t "}" }),
              sn(nil, { t "_{", i(1, "lower"), t "}", t "^{", i(1, "upper"), t "}" }),
            }
          )
        }
      )
    ),
    s("lm",        { t "$", i(1), t "$" }),
    s("und",       { t "\\underline{", i(1), t "}" }),
    s("cite",      { t "\\,\\cite{", i(1), t "}" }),
    s("foootnote", { t "\\footnote{", i(1), t "}" }),
    s("mono",      { t "\\texttt{", i(1), t "}" }),
    s("bld",       { t "\\textbf{", i(1), t "}" }),
    s("itl",       { t "\\textit{", i(1), t "}" }),
    s("emph",      { t "\\emph{", i(1), t "}" }),
    s("ref",       { t "\\ref{", i(1), t "}" }),
    s("fig",       { t "Figure \\ref{fig:", i(1), t "}" }),
  },
  autosnippets = {
    s("...", { t "\\dots" }),
    s("???", { t "\\,\\cite{citationNeeded}" }),
  },
}
