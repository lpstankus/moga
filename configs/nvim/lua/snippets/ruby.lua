local ls = require('luasnip')
local s, sn = ls.snippet, ls.snippet_node
local f, t, i, c, d = ls.function_node, ls.text_node, ls.insert_node, ls.choice_node, ls.dynamic_node
local rep = require("luasnip.extras").rep
local fmt = require("luasnip.extras.fmt").fmt

return {
  snippets = {
    s(
      'def',
      fmt(
        [[
          def {}{}
            {}
          end
        ]],
        {
          i(1, "name"),
          c(2, { t "", fmt([[({})]], { i(1) }) }),
          i(0),
        }
      )
    ),
    s("mock", fmt([[let (:{}) {{ double '{}' }}]], { i(1, 'mock'), rep(1) })),
    s(
      'let',
      fmt(
        [[let (:{}) {}]],
        {
          i(1, "var"),
          c(
            2,
            {
              fmt([[{{ {} }}]], { i(1) }),
              fmt(
                [[
                  do
                    {}
                  end
                ]],
                { i(1) }
              ),
            }
          ),
        }
      )
    ),
    s(
      'describe',
      fmt(
        [[
          describe '{}' do
            {}
          end
        ]],
        { i(1), i(0) }
      )
    ),
    s(
      'test',
      fmt(
        [[
          it 'is expected to {}' do
            {}
          end
        ]],
        { i(1, 'behaviour'), i(0) }
      )
    ),
  },
}
