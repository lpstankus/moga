local ls = require('luasnip')
local s, sn = ls.snippet, ls.snippet_node
local f, t, i, c, d = ls.function_node, ls.text_node, ls.insert_node, ls.choice_node, ls.dynamic_node
local rep = require("luasnip.extras").rep
local fmt = require("luasnip.extras.fmt").fmt

return {
  snippets = {
    s(
      'snippet_file',
      fmt(
        [[
          local ls = require('luasnip')
          local s, sn = ls.snippet, ls.snippet_node
          local f, t, i, c, d = ls.function_node, ls.text_node, ls.insert_node, ls.choice_node, ls.dynamic_node
          local rep = require("luasnip.extras").rep
          local fmt = require("luasnip.extras.fmt").fmt

          return {{
            snippets = {{}},
            autosnippets = {{}},
          }}
        ]],
        {}
      )
    ),
  },
  autosnippets = {},
}
