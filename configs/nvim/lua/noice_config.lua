require("noice").setup {
  lsp = {
    override = {
      ["vim.lsp.util.convert_input_to_markdown_lines"] = true,
      ["vim.lsp.util.stylize_markdown"] = true,
      ["cmp.entry.get_documentation"] = true,
    },
    hover = { enabled = true },
    signature = { enabled = false },
    progress = {
      enabled = true,
      format = "lsp_progress",
      format_done = "lsp_progress_done",
      throttle = 1000 / 30,
      view = "mini",
    },
  },

  cmdline = {
    enabled = true,
    view = "cmdline_popup",
    format = {
      cmdline = { title = "Command", pattern = "^:", icon = "run▕", lang = "vim" },
      replace = { title = "Replace", pattern = '^:%%s/', icon = "rep▕", lang = "regex" },
      help = { title = "Help", pattern = "^:%s*he?l?p?%s+", icon = "hlp▕" },
      lua = {
        pattern = { "^:%s*lua%s+", "^:%s*lua%s*=%s*", "^:%s*=%s*" },
        icon = "lua▕", lang = "lua"
      },
      search_down = { kind = "search",   pattern = "^/",   icon = "for▕", lang = "regex", view = "cmdline" },
      search_up   = { kind = "search",   pattern = "^%?",  icon = "bak▕", lang = "regex", view = "cmdline" },
    },
  },

  views = {
    hover = {
      silent = false,
      view = "popup",
      border = { style = "single", padding = { 0, 0 }, },
      position = { row = 2, col = 0 },
      win_options = {
        winhighlight = { Normal = "Normal", FloatBorder = "NoicePopupBorder" },
        linebreak = true,
        wrap = true,
      },
    },

    cmdline_popup = {
      border = { style = "single", padding = { 0, 0 } },
      win_options = {
        winhighlight = {
          Normal = "Normal",
          IncSearch = "Normal",
          CurSearch = "Normal",
          Search = "Normal",
        },
      },
      search_down = { view = "cmdline" },
      search_up   = { view = "cmdline" },
    },
  },
}
