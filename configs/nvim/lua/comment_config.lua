local ft = require('Comment.ft')
local utils = require('Comment.utils')

local post_hook = function(ctx)
  if ctx.range.srow == -1 or ctx.ctype ~= 2 then return end

  local padding = " "
  local lines = vim.api.nvim_buf_get_lines(0, ctx.range.srow - 1, ctx.range.erow, false)

  local cstr = ft.get(vim.bo.filetype, ctx.ctype)
  local lcs, rcs = utils.unwrap_cstr(cstr)

  if ctx.cmode == 1 then
    local str = lines[1]
    local i, j = string.find(str, lcs .. padding, 1, true)
    lines[1] = string.sub(str, 1, i - 1) .. string.sub(str, j + 1, #str)
    table.insert(lines, 1, string.sub(str, i, j - 1))

    local str = lines[#lines]
    local i, j = string.find(str, padding .. rcs, 1, true)
    lines[#lines] = string.sub(str, 1, i - 1) .. string.sub(str, j + 1, #str)
    table.insert(lines, #lines + 1, string.sub(str, i + 1, j))
  elseif ctx.cmode == 2 then
    -- uncomment
    if #lines[1] == 0 and #lines[#lines] == 0 then
      table.remove(lines, 1)
      table.remove(lines, #lines)
    end
  end

  vim.api.nvim_buf_set_lines(0, ctx.range.srow - 1, ctx.range.erow, false, lines)
end


require('Comment').setup {
  padding = true,
  ignore = '^$',
  toggler = {
    line = 'gcc',
    block = 'gbc',
  },
  opleader = {
    line = 'gc',
    },
    mappings = {
        basic = true,
        extra = false,
    block = 'gb',
    extended = false,
  },
  pre_hook = nil,
  post_hook = post_hook,
}
