local colors = {
  color0  = '#00161d',
  color1  = '#052029',
  color2  = '#122b34',
  color3  = '#031b23',
  color4  = '#d1b896',
  color5  = '#e8dccb',
  color6  = '#7aa6ff',
  color7  = '#8cde94',
  color8  = '#2ec09c',
  color9  = '#7ad0c6',
  color10 = '#c03b3b',
  color11 = '#bf616a',
  color12 = '#d0675b',
}

local sla = {
  normal = {
    a = { fg = colors.color0, bg = colors.color11, gui = 'bold' },
    b = { fg = colors.color0, bg = colors.color4                },
    c = { fg = colors.color4, bg = colors.color1                },
  },
  insert = {
    a = { fg = colors.color0, bg = colors.color9,  gui = 'bold' },
    b = { fg = colors.color0, bg = colors.color4                },
    c = { fg = colors.color4, bg = colors.color1                },
  },
  visual = {
    a = { fg = colors.color0, bg = colors.color6,  gui = 'bold' },
    b = { fg = colors.color0, bg = colors.color4                },
    c = { fg = colors.color4, bg = colors.color1                },
  },
  replace = {
    a = { fg = colors.color0, bg = colors.color7,  gui = 'bold' },
    b = { fg = colors.color0, bg = colors.color4                },
    c = { fg = colors.color4, bg = colors.color1                },
  },
  command = {
    a = { fg = colors.color0, bg = colors.color12, gui = 'bold' },
    b = { fg = colors.color0, bg = colors.color4                },
    c = { fg = colors.color4, bg = colors.color1                },
  },
  inactive = {
    a = { fg = colors.color4, bg = colors.color0,  gui = 'bold' },
    b = { fg = colors.color4, bg = colors.color0                },
    c = { fg = colors.color4, bg = colors.color0                },
  },
}

local theme = sla
if vim.g.colors_name ~= "sla" then theme = "auto" end

require("lualine").setup {
  options = {
    theme = theme,
    icons_enabled = true,
    component_separators = { left = '│', right = '│'},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
    globalstatus = true,
  },
  sections = {
    lualine_a = { 'mode' },
    lualine_b = { 'branch' },
    lualine_c = { 'filename' },
    lualine_x = { 'encoding', 'fileformat' },
    lualine_y = { 'filetype', 'progress' },
    lualine_z = { 'location' }
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {}
}
