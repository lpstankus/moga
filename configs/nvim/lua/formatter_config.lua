local from_stdin = function(cmd)
  return function()
    return {
      exe = "cat",
      args = {
        vim.api.nvim_buf_get_name(0),
        "| " .. cmd,
        "|| cat",
        vim.api.nvim_buf_get_name(0),
      },
      stdin = true,
    }
  end
end

local clang = function()
  return {
    exe = "clang-format",
    args = { "--assume-filename", vim.api.nvim_buf_get_name(0) },
    stdin = true,
    cwd = vim.fn.expand('%:p:h'),
  }
end

local prettier = function()
  return {
    exe = "prettier",
    args = { vim.api.nvim_buf_get_name(0) },
    stdin = true,
  }
end

require('formatter').setup {
  filetype = {
    cpp = { clang },
    cuda = { clang },
    glsl = { clang },
    javascript = { prettier },
    typescript = { prettier },
    html = { prettier },
    css = { prettier },
    python = { from_stdin("black -") },
    rust = { from_stdin("rustfmt --emit stdout --quiet --edition 2018") },
    zig = { from_stdin("zig fmt --stdin") },
    odin = { from_stdin("odinfmt -stdin") },
    go = {
      function()
        return {
          exe = "gofmt",
          args = {"-s", vim.api.nvim_buf_get_name(0)},
          stdin = true,
        }
      end
    },
  },
}

vim.cmd [[
  augroup personal_neoformatter
    au!
    au BufWritePost * FormatWrite
  augroup END
]]
