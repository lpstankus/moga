local remap = vim.api.nvim_set_keymap
local npairs = require('nvim-autopairs')

npairs.setup({
  ignored_next_char = "[%w%.]",
  map_bs = false,
})

_G.MUtils= {}

MUtils.CR = function()
  if vim.fn.pumvisible() ~= 0 then
    if vim.fn.complete_info({ 'selected' }).selected ~= -1 then
      return npairs.esc('<c-y>')
    end
    return npairs.esc('<c-e>') .. npairs.autopairs_cr()
  end
  return npairs.autopairs_cr()
end
remap('i', '<cr>', 'v:lua.MUtils.CR()', { expr = true, noremap = true })

MUtils.BS = function()
  if vim.fn.pumvisible() ~= 0 and vim.fn.complete_info({ 'mode' }).mode == 'eval' then
    return npairs.esc('<c-e>') .. npairs.autopairs_bs()
  end
  return npairs.autopairs_bs()
end
remap('i', '<bs>', 'v:lua.MUtils.BS()', { expr = true, noremap = true })
