local mapper = function(mode, key, fn, bufnr)
  vim.keymap.set(mode, key, fn, { buffer = bufnr, noremap = true, silent = true })
end

require('gitsigns').setup {
  signs = {
    add          = { text = '┃' },
    untracked    = { text = '┃' },
    change       = { text = '┃' },
    delete       = { text = '┃' },
    topdelete    = { text = '┃' },
    changedelete = { text = '┃' },
  },
  signs_staged = {
    add          = { text = '┃' },
    untracked    = { text = '┃' },
    change       = { text = '┃' },
    delete       = { text = '┃' },
    topdelete    = { text = '┃' },
    changedelete = { text = '┃' },
  },
  signcolumn = true,
  numhl      = false,
  linehl     = false,
  word_diff  = false,
  watch_gitdir = {
    interval = 1000,
    follow_files = true
  },
  attach_to_untracked = true,
  current_line_blame = true,
  current_line_blame_opts = {
    virt_text = true,
    virt_text_pos = 'eol',
    delay = 1000,
  },
  current_line_blame_formatter = '<author>, <author_time:%Y-%m-%d> - <summary>',
  sign_priority = 6,
  update_debounce = 100,
  max_file_length = 40000,
  preview_config = {
    border = 'single',
    style = 'minimal',
    relative = 'cursor',
    row = 0,
    col = 1
  },
  on_attach = function(buffnr)
  local gs = package.loaded.gitsigns
    mapper('n', '<leader>hn', gs.next_hunk,       buffnr)
    mapper('n', '<leader>hp', gs.prev_hunk,       buffnr)
    mapper('n', '<leader>hd', gs.preview_hunk,    buffnr)
    mapper('n', '<leader>hS', gs.stage_buffer,    buffnr)
    mapper('n', '<leader>hs', gs.stage_hunk,      buffnr)
    mapper('n', '<leader>hu', gs.undo_stage_hunk, buffnr)
    mapper('n', '<leader>hR', gs.reset_buffer,    buffnr)
    mapper('n', '<leader>hr', gs.reset_hunk,      buffnr)
    mapper('n', '<leader>gb', function() gs.blame_line{ full=true } end, buffnr)
    mapper('v', '<leader>hs', function() gs.stage_hunk({ vim.fn.line("."), vim.fn.line("v") }) end, buffnr)
    mapper('v', '<leader>hr', function() gs.reset_hunk({ vim.fn.line("."), vim.fn.line("v") }) end, buffnr)
  end,
}
