local lspkind = require('lspkind')
lspkind.init {
  symbol_map = {
    Copilot        = "ai",
    Text           = "tx",
    String         = "tx",
    Comment        = "tx",
    Keyword        = "kw",
    KeywordReturn  = "kw",
    Operator       = "op",
    Constructor    = "fn",
    Method         = "fn",
    Function       = "fn",
    FunctionCall   = "fn",
    Variable       = "vr",
    VariableMember = "vr",
    Constant       = "ct",
    Value          = "ct",
    Enum           = "en",
    EnumMember     = "en",
    Struct         = "tp",
    TypeParameter  = "tp",
    Class          = "tp",
    Field          = "fd",
    Module         = "md",
    Interface      = "in",
    Property       = "pr",
    Event          = "ev",
    Snippet        = "sn",
    Reference      = "rf",
    Color          = "cl",
    Unit           = "un",
    File           = "fi",
    Folder         = "fi",
  },
}

local cmp = require('cmp')

require("copilot").setup { suggestion = { enabled = false }, panel = { enabled = false }, }
require("copilot_cmp").setup()

cmp.setup {
  view = {
    entries = "custom",
    auto_open = true,
  },
  window = {
    documentation = cmp.config.window.bordered {
      winhighlight = "Normal:CmpPmenu,CursorLine:PmenuSel,Search:None",
      border = { '┌', '─', '┐', '│', '┘','─', '└', '│' },
      scrolloff = 3,
    },
    completion = cmp.config.window.bordered {
      winhighlight = "Normal:CmpPmenu,CursorLine:PmenuSel,Search:None",
      border = { '┌', '─', '┐', '│', '┘','─', '└', '│' },
      side_padding = 0,
      col_offset = 0,
      scrolloff = 3,
    },
  },
  mapping = {
    ['<c-space>'] = cmp.mapping.complete(),
    ["<tab>"] = cmp.config.disable,
    ["<c-n>"] = cmp.mapping.select_next_item { behavior = cmp.SelectBehavior.Insert },
    ["<c-p>"] = cmp.mapping.select_prev_item { behavior = cmp.SelectBehavior.Insert },
    ['<c-d>'] = cmp.mapping.scroll_docs(4),
    ['<c-u>'] = cmp.mapping.scroll_docs(-4),
    ['<c-e>'] = cmp.mapping.close(),
    ['<c-y>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Insert,
      select = true,
    },
  },
  snippet = {
    expand = function(args)
      require("luasnip").lsp_expand(args.body)
    end,
  },
  sources = {
    { name = "copilot" },
    { name = "nvim_lsp" },
    { name = "nvim_lsp_signature_help" },
    { name = "luasnip" },
    { name = "path" },
    { name = 'calc' },
    { name = "latex_symbols" },
    { name = "treesitter" },
    { name = "nvim_lua" },
    { name = "buffer" },
  },
  formatting = {
    fields = { "kind", "abbr", "menu" },
    format = function(entry, vim_item)
      local kind = require("lspkind").cmp_format {
        mode = "symbol_text",
        menu = {
          copilot       = "COP",
          luasnip       = "SNP",
          nvim_lsp      = "LSP",
          path          = "PAT",
          treesitter    = "TSS",
          buffer        = "BUF",
          calc          = "CAL",
          nvim_lua      = "LUA",
          latex_symbols = "LAT",
        },
      } (entry, vim_item)

      kind.kind_hl_group = nil
      kind.kind = " " .. (kind.kind:match("[^%s]+") or "..") .. " "
      kind.menu = "│  " .. (kind.menu or "???")
      kind.abbr = kind.abbr:match("[^(]+")

      return kind
    end,
  },
	sorting = {
		comparators = {
			cmp.config.compare.offset,
			cmp.config.compare.exact,
      require("copilot_cmp.comparators").prioritize,
			cmp.config.compare.score,
			cmp.config.compare.recently_used,
			-- require("cmp-under-comparator").under,
			cmp.config.compare.kind,
		},
	},
  experimental = { ghost_text = true },
}
