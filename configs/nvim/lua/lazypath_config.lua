local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
print(lazypath)
if not vim.loop.fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable",
    lazypath,
  })
end
vim.opt.rtp:prepend(lazypath)

local plugins = {
  -- Colorschemes
  "ayu-theme/ayu-vim",
  "joshdick/onedark.vim",
  "kdheepak/monochrome.nvim",
  "morhetz/gruvbox",
  "pgdouyon/vim-yin-yang",
  "rebelot/kanagawa.nvim",
  -- "lpstankus/sla",
  { dir = "~/Documents/Projects/themes/sla" },
  { dir = "~/Documents/Projects/themes/soneca" },

  -- Filetype
  "DingDean/wgsl.vim",
  "Tetralux/odin.vim",
  "lervag/vimtex",
  "prisma/vim-prisma",
  "rust-lang/rust.vim",
  "simrat39/rust-tools.nvim",
  "tikhomirov/vim-glsl",
  "ziglang/zig.vim",

  -- Visual stuff
  { "lukas-reineke/indent-blankline.nvim", main = "ibl" },
  "brenoprata10/nvim-highlight-colors",
  "folke/todo-comments.nvim",
  "kyazdani42/nvim-web-devicons",
  "m4xshen/smartcolumn.nvim",
  "nvim-lualine/lualine.nvim",
  "nvim-treesitter/nvim-treesitter-context",
  -- {
  --   "folke/noice.nvim", event = "VeryLazy",
  --   dependencies = {
  --     "MunifTanjim/nui.nvim",
  --     "rcarriga/nvim-notify",
  --   },
  -- },

  -- Commands and autocommands
  "NMAC427/guess-indent.nvim",
  "airblade/vim-rooter",
  "tpope/vim-dispatch",

  -- Extensions
  "mbbill/undotree",
  "reedes/vim-pencil",
  { dir = "~/Documents/Projects/plugins/qck" },

  -- Text manipulation
  "junegunn/vim-easy-align",
  "machakann/vim-sandwich",
  "mhartington/formatter.nvim",
  "mizlan/iswap.nvim",
  "numToStr/Comment.nvim",
  "windwp/nvim-autopairs",

  -- Movement
  { "ThePrimeagen/harpoon", branch = "harpoon2" },
  {
    "nvim-telescope/telescope.nvim",
    dependencies = {
      "nvim-lua/plenary.nvim",
      "nvim-lua/popup.nvim",
      "nvim-telescope/telescope-fzy-native.nvim",
    },
  },

  -- Treesitter
  { "nvim-treesitter/nvim-treesitter", build = ":TSUpdate" },
  "nvim-treesitter/playground",

  -- Copilot
  "zbirenbaum/copilot.lua",

  -- Completion engine
  "hrsh7th/nvim-cmp",

  -- Completion sources
  "hrsh7th/cmp-buffer",
  "hrsh7th/cmp-nvim-lsp",
  "hrsh7th/cmp-nvim-lua",
  "hrsh7th/cmp-path",
  "kdheepak/cmp-latex-symbols",
  "ray-x/cmp-treesitter",
  "saadparwaiz1/cmp_luasnip",
  "zbirenbaum/copilot-cmp",

  -- Snippets
  { "chrisgrieser/nvim-scissors", opts = { snippetDir = "~/.config/nvim/snippets/", } },
  "L3MON4D3/LuaSnip",

  -- Lsp
  "neovim/nvim-lspconfig",
  "onsails/lspkind-nvim",
  "ray-x/lsp_signature.nvim",
  "williamboman/mason.nvim",
  "williamboman/mason-lspconfig.nvim",

  -- Diagnostics
  -- "lpstankus/bob-nvim",
  { dir = "~/Documents/Projects/plugins/bob-nvim" },
  "folke/trouble.nvim",

  -- Vcs
  "lewis6991/gitsigns.nvim",
  "sindrets/diffview.nvim",
  "NeogitOrg/neogit",
}
require("lazy").setup(plugins)
