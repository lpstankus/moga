local parser_config = {
  go = {
    pattern = "^(.+):([0-9]+):([0-9]+): (.+)",
    groups = { "file", "lnum", "col",  "message" },
    error_map = {},
  },
  zig = {
    pattern = "^(.+):([0-9]+):([0-9]+): (.+):(.+)",
    groups = { "file", "lnum", "col", "severity", "message" },
    error_map = {
      ["error"] = vim.diagnostic.severity.ERROR,
      ["note"] = vim.diagnostic.severity.INFO,
    }
  },
  luacheck = {
    pattern = "^ *(.+):([0-9]+):([0-9]+): .([EW]).+ (.+)",
    groups = { "file", "lnum", "col", "severity", "message" },
    error_map = {
      ["E"] = vim.diagnostic.severity.ERROR,
      ["W"] = vim.diagnostic.severity.WARN,
    }
  }
}

require("bob").setup {
  builders = {
    go_run = { cmd = "go run ./src/main.go", parser = parser_config.go },
    zig_run = { cmd = "zig build run", parser = parser_config.zig },
    internasusto = { cmd = "HEALTHDASHBOARD_PUBLIC=true ./bin/dev" },
  },
  linters = {
    zig_check = {
      cmd = "zig build check",
      parser = parser_config.zig,
      stream = "stderr",
    },
    luacheck = {
      cmd = "luacheck .",
      parser = parser_config.luacheck,
      stream = "stdout",
    }
  },
}

local bob_group = vim.api.nvim_create_augroup("Bob", {})
vim.api.nvim_create_autocmd(
  { "BufWritePost" },
  { group = bob_group, pattern = "*", callback = function() require("bob").lint() end }
)

vim.keymap.set("n", "<leader>bb", function(_) require("bob").build({ open_win = true, force_new = false }) end, { noremap = true })
vim.keymap.set("n", "<leader>bf", function(_) require("bob").build({ open_win = true, force_new = true  }) end, { noremap = true })
vim.keymap.set("n", "<leader>bs", function(_) require("bob").toggle_window() end, { noremap = true })
vim.keymap.set("n", "<leader>bk", function(_) require("bob").kill_builder() end, { noremap = true })
