require("mason").setup { ui = { border = "single" } }
require("mason-lspconfig").setup {
  ensure_installed = { "pyright", "vimls", "wgsl_analyzer", "ols", "lua_ls", "gopls", "tsserver", "prismals", "solargraph"}
}

local lsp = require('lspconfig')
local rt = require('rust-tools')

local mapper = function(mode, key, fn, bufnr)
  vim.keymap.set(mode, key, fn, { buffer = bufnr, noremap = true, silent = true })
end

---------- On attach -----------------------------------------------------------

local on_attach = function(_, bufnr)
  mapper('n', '<leader>lD', vim.lsp.buf.declaration,    bufnr)
  mapper('n', '<leader>ld', vim.lsp.buf.definition,     bufnr)
  mapper('n', '<leader>li', vim.lsp.buf.implementation, bufnr)
  mapper('n', '<leader>lh', vim.lsp.buf.hover,  bufnr)
  mapper('n', '<leader>lr', vim.lsp.buf.rename, bufnr)
end

---------- Servers -------------------------------------------------------------

-- Servers that don't require custom configuration
local servers = { "pyright", "vimls", "zls", "wgsl_analyzer", "ols", "gopls", "tsserver", "prismals" }
for _, server in ipairs(servers) do lsp[server].setup { on_attach = on_attach } end

-- clangd
lsp.clangd.setup {
  on_attach = on_attach,
  cmd = {
    "clangd",
    "--offset-encoding=utf-8",
  },
}

-- Lua
lsp.lua_ls.setup {
  on_init = function(client)
    local path = client.workspace_folders[1].name
    if vim.loop.fs_stat(path..'/.luarc.json') or vim.loop.fs_stat(path..'/.luarc.jsonc') then
      return
    end
    client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
      runtime = { version = 'LuaJIT' },
      workspace = {
        checkThirdParty = false,
        library = {
          vim.env.VIMRUNTIME
        }
      }
    })
  end,
  on_attach = on_attach,
  settings = { Lua = {} }
}

-- Rust tools
rt.setup {
  server = {
    settings = {
      ["rust-analyzer"] = {
        inlayHints = { locationLinks = false },
      },
    },
    on_attach = function(client, bufnr)
      on_attach(client, bufnr)

      rt.inlay_hints.enable()

      mapper('n', '<leader>lh', rt.hover_actions.hover_actions, bufnr)
      mapper('n', '<leader>lm', rt.expand_macro.expand_macro,   bufnr)
      mapper('n', '<leader>lp', rt.parent_module.parent_module, bufnr)
    end,
  },
  tools = {
    hover_actions = {
      border = {
        { "┌", "FloatBorder" },
        { "─", "FloatBorder" },
        { "┐", "FloatBorder" },
        { "│", "FloatBorder" },
        { "┘", "FloatBorder" },
        { "─", "FloatBorder" },
        { "└", "FloatBorder" },
        { "│", "FloatBorder" },
      },
    },
  },
}

-- Ruby
lsp.solargraph.setup {
  settings = { solargraph = { diagnostics = { true } } },
  on_attach = on_attach,
}

vim.lsp.handlers['textDocument/hover'] = vim.lsp.with(vim.lsp.handlers.hover, { border = 'single' })
vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with(vim.lsp.handlers.signature_help, { border = 'single' })
