
---------- Init ----------------------------------------------------------------

local ls = require('luasnip')
local types = require("luasnip.util.types")

ls.config.set_config({
  history = true,
  updateevents = "TextChanged,TextChangedI",
  enable_autosnippets = true,
  ext_opts = {
    [types.choiceNode] = {
      active = {
        virt_text = { { " <<-- ", "Normal" } },
      },
    },
  },
})

---------- Load snippets -------------------------------------------------------

function force_require(file)
  for k in pairs(package.loaded) do
    if k:match("^" .. file) then package.loaded[k] = nil end
  end
  return require(file)
end

-- Load vscode style snippets under ~/.config/nvim/snippets/
local vscodesnips = "~/.config/nvim/snippets/"
require("luasnip/loaders/from_vscode").lazy_load({ paths = vscodesnips })

-- Load all lua snippets under ~/config/nvim/snippets/lua
local luasnips = debug.getinfo(1).source:match("@(.*/)") .. "snippets"
local files = require('plenary.scandir').scan_dir(luasnips, { hidden = false, add_dirs = false })
for _, file in ipairs(files) do
  local filetype = file:match("^.+/(.+)%..+$")
  local snips = force_require('snippets.' .. filetype)
  ls.add_snippets(filetype, snips.snippets)
  ls.add_snippets(filetype, snips.autosnippets, { type = "autosnippets" })
end

---------- keymaps -------------------------------------------------------------

vim.keymap.set(
  { "i", "s" }, "<c-k>", function()
    if ls.expand_or_jumpable() then
      ls.expand_or_jump()
    end
  end,
  { silent = true }
)

vim.keymap.set(
  { "i", "s" }, "<c-j>", function()
    if ls.jumpable(-1) then
      ls.jump(-1)
    end
  end,
  { silent = true }
)

vim.keymap.set(
  { "i", "s" }, "<c-l>", function()
    if ls.choice_active() then
      ls.change_choice(1)
    end
  end
)

vim.keymap.set(
  { "i", "s" }, "<c-h>", function()
    if ls.choice_active() then
      ls.change_choice(-1)
    end
  end
)

vim.keymap.set("n", "<leader><leader>s", "<cmd>source ~/.config/nvim/lua/luasnip_config.lua<cr>")

function leave_snippet()
  if (ls.session.current_nodes[vim.api.nvim_get_current_buf() and not ls.in_snippet() ]) then
    require('luasnip').unlink_current()
  end
end

-- stop snippets when you leave to normal mode
vim.api.nvim_command([[autocmd CursorMoved * lua leave_snippet()]])

-- From LuaSnip wiki:
local current_nsid = vim.api.nvim_create_namespace("LuaSnipChoiceListSelections")
local current_win = nil

local function window_for_choiceNode(choiceNode)
  local buf = vim.api.nvim_create_buf(false, true)
  local buf_text = {}
  local row_selection = 0
  local row_offset = 0
  local text
  for _, node in ipairs(choiceNode.choices) do
    text = node:get_docstring()
    -- find one that is currently showing
    if node == choiceNode.active_choice then
      -- current line is starter from buffer list which is length usually
      row_selection = #buf_text
      -- finding how many lines total within a choice selection
      row_offset = #text
    end
    vim.list_extend(buf_text, text)
  end

  vim.api.nvim_buf_set_text(buf, 0,0,0,0, buf_text)
  local w, h = vim.lsp.util._make_floating_popup_size(buf_text)

  -- adding highlight so we can see which one is been selected.
  local extmark = vim.api.nvim_buf_set_extmark(
    buf,
    current_nsid,
    row_selection,
    0,
    { hl_group = 'incsearch',end_line = row_selection + row_offset }
  )

  -- shows window at a beginning of choiceNode.
  local win = vim.api.nvim_open_win(buf, false, {
    relative = "win", width = w, height = h, bufpos = choiceNode.mark:pos_begin_end(), style = "minimal", border = 'single'
  })

  -- return with 3 main important so we can use them again
  return {win_id = win,extmark = extmark,buf = buf}
end

function choice_popup(choiceNode)
  -- build stack for nested choiceNodes.
  if current_win then
    vim.api.nvim_win_close(current_win.win_id, true)
    vim.api.nvim_buf_del_extmark(current_win.buf,current_nsid,current_win.extmark)
  end
  local create_win = window_for_choiceNode(choiceNode)
  current_win = {
    win_id = create_win.win_id,
    prev = current_win,
    node = choiceNode,
    extmark = create_win.extmark,
    buf = create_win.buf
  }
end

function update_choice_popup(choiceNode)
  vim.api.nvim_win_close(current_win.win_id, true)
  vim.api.nvim_buf_del_extmark(current_win.buf,current_nsid,current_win.extmark)
  local create_win = window_for_choiceNode(choiceNode)
  current_win.win_id = create_win.win_id
  current_win.extmark = create_win.extmark
  current_win.buf = create_win.buf
end

function choice_popup_close()
  vim.api.nvim_win_close(current_win.win_id, true)
  vim.api.nvim_buf_del_extmark(current_win.buf,current_nsid,current_win.extmark)
  -- now we are checking if we still have previous choice we were in after exit nested choice
  current_win = current_win.prev
  if current_win then
    -- reopen window further down in the stack.
    local create_win = window_for_choiceNode(current_win.node)
    current_win.win_id = create_win.win_id
    current_win.extmark = create_win.extmark
    current_win.buf = create_win.buf
  end
end

vim.cmd [[
  augroup choice_popup
  au!
  au User LuasnipChoiceNodeEnter lua choice_popup(require("luasnip").session.event_node)
  au User LuasnipChoiceNodeLeave lua choice_popup_close()
  au User LuasnipChangeChoice lua update_choice_popup(require("luasnip").session.event_node)
  augroup END
]]
