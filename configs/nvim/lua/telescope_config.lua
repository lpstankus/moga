local actions = require('telescope.actions')
local telescope = require('telescope')

telescope.setup {
  defaults = {
    prompt_prefix = '> ',
    borderchars = { '─', '│', '─', '│', '┌', '┐', '┘', '└'},
    mappings = {
      i = { ["<c-c>"] = false },
      n = { ["<c-c>"] = actions.close }
    }
  },
  extensions = {
    fzy_native = {
      override_generic_sorter = false,
      override_file_sorter = true,
    }
  }
}
telescope.load_extension('fzy_native')
