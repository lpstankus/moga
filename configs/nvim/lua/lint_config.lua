local lint = require("lint")

lint.linters = {
  zig_compile = {
    cmd = "zig",
    args = { "build", "check" },
    cwd = vim.fn.getcwd(),
    stdin = false,
    append_fname = false,
    stream = "stderr",
    ignore_exitcode = true,
    parser = require('lint.parser').from_pattern(
      "^(.+):([0-9]+):([0-9]+): (.+):(.+)",
      { "file", "lnum", "col", "severity", "message" },
      {
        ["error"] = vim.diagnostic.severity.ERROR,
        ["note"] = vim.diagnostic.severity.INFO,
      },
      { ["source"] = "zig compile" },
      {}
    ),
  }
}

lint.linters_by_ft = {
  zig = { "zig_compile" }
}

vim.cmd [[
  augroup personal_lint
    au!
    au BufWritePost * lua require('lint').try_lint()
  augroup END
]]
