syntax match odinType "\v<[]>[A-Z]\w+"
hi! link odinType Type

syntax keyword odinConstant nil
syntax match odinConstant "\v<[A-Z0-9_]+>"
hi! link odinConstant Constant

syntax match odinFn "\v<\w+>(\s*\()@="
hi! link odinFn Function

syntax keyword odinKeyword auto_cast
hi! link odinKeyword keyword
