syntax match rustType "\v<[]>[A-Z]\w+"
hi! link rustType Type

syntax match rustConstant "\v<[A-Z][A-Z0-9_]*>"
hi! link rustConstant Constant

syntax keyword rustOperator _
hi! link rustOperator Operator
