syntax match wgslType "\v<[]>[A-Z]\w+"
hi! link wgslType Type

syntax keyword wgslKeyword struct
hi! link wgslKeyword Keyword
