syntax match zigType "\v<[]>[A-Z]\w+"
hi! link zigType Type

syntax match zigConstant "\v<[A-Z0-9_]+>"
hi! link zigConstant Constant

syntax match zigFn "\v<\w+>(\s*\()@="
hi! link zigFn Function
