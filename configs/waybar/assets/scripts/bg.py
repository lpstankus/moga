import random as rand
import png

def hex_to_rgb(hex):
    hex = hex.lstrip('#')
    img = []
    for i in (0, 2, 4): img.append(int(hex[i:i + 2], 16))
    return img

size = (3200, 50)

# threshold = 0.95
# file = "bg.png"
# alpha = 255

# threshold = 0.95
# file = "bg_transparent.png"
# alpha = 120

# color_a = hex_to_rgb("#2e3440")
# color_b = hex_to_rgb("#65444f")
# color_c = False

# color_a = hex_to_rgb("#C5D6E2")
# color_b = hex_to_rgb("#B3D1DE")
# color_c = False

threshold  = 0.2
threshold2 = 0.05
file = "workspace.png"
alpha = 255

color_a = hex_to_rgb("#b6c18b")
color_b = hex_to_rgb("#ebcb8b")
color_c = hex_to_rgb("#8e7265")

noise = []
for i in range(size[1]):
    noise.append([])
    for j in range(size[0]):
        noise[i].append(rand.random())

img = []
for i in range(size[1]):
    img.append([])
    for j in range(size[0]):
        col = color_a if noise[i][j] < threshold else color_b
        if color_c != False and noise[i][j] < threshold2: col = color_c
        for chan in col: img[i].append(chan)
        img[i].append(alpha)

with open(f"assets/{file}", "wb") as f:
    w = png.Writer(size[0], size[1], greyscale=False, alpha=True)
    w.write(f, img)
