-- Awesome Libs
local gears = require("gears")
local awful = require("awful")

root.buttons = gears.table.join(
  awful.button({ }, 3, function() mymainmenu:toggle() end),
  awful.button({ modkey }, 4, awful.tag.viewprev),
  awful.button({ modkey }, 5, awful.tag.viewnext),
)
