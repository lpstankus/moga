local awful = require("awful")
local gears = require("gears")
local modkey = user_vars.modkey

return gears.table.join(
  awful.button(
    {}, 1,
    function(c) c:activate { context = "mouse_click" } end
  ),
  awful.button(
    { modkey }, 1,
    function(c) c:activate { context = "mouse_click", action = "mouse_move"  } end
  ),
  awful.button(
    { modkey }, 2,
    function(c) c:kill() end
  ),
  awful.button(
    { modkey }, 3,
    function(c) c:activate { context = "mouse_click", action = "mouse_resize"} end
  )
)
