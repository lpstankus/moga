local awful = require("awful")
local gears = require("gears")
local modkey = user_vars.modkey

return gears.table.join(
  awful.key(
    { modkey, "Shift" }, "c",
    function(c) c:kill() end,
    { description = "close", group = "client" }
  ),
  awful.key(
    { modkey }, "f",
    function(c)
      c.fullscreen = not c.fullscreen
      c:raise()
    end,
    { description = "toggle fullscreen", group = "client" }
  ),
  awful.key(
    { modkey }, "t",
    function(c) c:swap(awful.client.getmaster()) end,
    { description = "move to master", group = "client" }
  ),
  awful.key(
    { modkey }, "p",
    function(c) c.ontop = not c.ontop end,
    { description = "toggle keep on top", group = "client" }
  ),
  awful.key(
    { modkey, "Control" }, "space",
    awful.client.floating.toggle,
    { description = "toggle floating", group = "client" }
  ),
  awful.key(
    { modkey }, "n",
    function(c) c.minimized = true end,
    { description = "minimize", group = "client" }
  ),
  awful.key(
    { modkey }, "m",
    function(c)
      c.maximized = not c.maximized
      c:raise()
    end,
    { description = "toggle maximize", group = "client" }
  ),
  awful.key(
    { modkey, "Shift", "Control" }, "l",
    function(c) c:move_to_screen(c.screen.index + 1) end,
    { description = "move to next screen", group = "client" }
  ),
  awful.key(
    { modkey, "Shift", "Control" }, "h",
    function(c) c:move_to_screen(c.screen.index - 1) end,
    { description = "move to previous screen", group = "client" }
  ),
)
