local awful = require("awful")
local ruled = require("ruled")

-- Rules to apply to new clients.
ruled.client.connect_signal("request::rules", function()
    -- All clients will match this rule.
    ruled.client.append_rule {
        id         = "global",
        rule       = { },
        properties = {
            focus     = awful.client.focus.filter,
            raise     = true,
            screen    = awful.screen.preferred,
            placement = awful.placement.no_overlap+awful.placement.no_offscreen
        }
    }

    -- Terminals
    ruled.client.append_rule {
        id = "terminals",
        rule_any = { class = { "kitty", "Alacritty" } },
        properties = { terminal = true },
    }

    -- Floating clients.
    ruled.client.append_rule {
        id       = "floating",
        rule_any = {
            instance = { "copyq", "pinentry" },
            class    = {
                "Arandr", "Blueman-manager", "Gpick", "Kruler", "Sxiv",
                "Tor Browser", "Wpa_gui", "veromix", "xtightvncviewer"
            },
            -- Note that the name property shown in xprop might be set slightly after creation of the client
            -- and the name shown there might not match defined rules here.
            name    = {
                "Event Tester",  -- xev.
            },
            role    = {
                "AlarmWindow",    -- Thunderbird's calendar.
                "ConfigManager",  -- Thunderbird's about:config.
                "pop-up",         -- e.g. Google Chrome's (detached) Developer Tools.
            }
        },
        properties = { floating = true }
    }

    -- Add titlebars to normal clients and dialogs
    ruled.client.append_rule {
        id         = "titlebars",
        rule_any   = { type = { "normal", "dialog" } },
        properties = { titlebars_enabled = true      }
    }
end)

-- local find_client_relative_index = function(c)
--     local index_table = awful.client.idx(c)
--     if index_table.col == 0 then
--         return index_table.col + index_table.idx
--     else
--         return index_table.idx - index_table.num
--     end
-- end

-- function if_parent_callback(child_pid, parent_pid, callback)
--     local cmd = string.format("pstree -ps %s", child_pid)
--     awful.spawn.easy_async(cmd, function(stdout)
--         if stdout:find('(' .. parent_pid .. ')') then callback() end
--         return
--     end)
-- end

-- client.connect_signal("manage", function(c)
--     if c.terminal then return end

--     if c.type == "normal" then
--         last_client = awful.client.focus.history.get(c.screen, 1)
--         if not last_client or not last_client.terminal then return end

--         if_parent_callback(c.pid, last_client.pid, function()
--             local term = last_client

--             c:geometry(term:geometry())
--             term:swap(c)
--             term.hidden = true

--             term.relative_index = find_client_relative_index(c)
--             c:connect_signal("swapped", function(_c)
--                 term.relative_index = find_client_relative_index(_c)
--             end)

--             c:connect_signal("unmanage", function(_c)
--                 term:geometry(_c:geometry())
--                 term.hidden = false
--                 term:activate{}
--                 awful.client.swap.byidx(term.relative_index, term)
--             end)
--         end)
--     end
-- end)
