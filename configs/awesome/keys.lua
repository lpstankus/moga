local awful = require("awful")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

-- {{{ Key bindings

-- Awesome {{{

awful.keyboard.append_global_keybindings{
  awful.key({ modkey            }, "s", hotkeys_popup.show_help,
            { description="show help", group="awesome" }),
  awful.key({ modkey, "Shift"   }, "r", awesome.restart,
            { description = "reload awesome", group = "awesome" }),
  awful.key({ modkey, "Control" }, "del", awesome.quit,
            { description = "quit awesome", group = "awesome" }),
}

-- }}}
-- Launcher {{{

awful.keyboard.append_global_keybindings{
  awful.key({ modkey }, "Return", function() awful.spawn(terminal) end,
            { description = "open a terminal", group = "launcher" }),
  awful.key({ modkey }, "r", function() awful.screen.focused().mypromptbox:run() end,
            { description = "run prompt", group = "launcher" }),
  awful.key({ modkey }, "p", function() menubar.show() end,
            { description = "show the menubar", group = "launcher" }),
}

-- }}}
-- Intra-Tag {{{

awful.keyboard.append_global_keybindings{
  awful.key({ modkey, "Mod1"             }, "Tab", function() awful.client.focus.byidx( 1) end,
            { description = "focus next by index", group = "client" }),
  awful.key({ modkey, "Shift", "Mod1"    }, "Tab", function() awful.client.focus.byidx(-1) end,
            { description = "focus previous by index", group = "client" }),
  awful.key({ modkey                     }, "j",   function() awful.client.focus.byidx( 1) end,
            { description = "focus next by index", group = "client" }),
  awful.key({ modkey                     }, "k",   function() awful.client.focus.byidx(-1) end,
            { description = "focus previous by index", group = "client" }),
  awful.key({ modkey, "Shift"            }, "j",   function() awful.client.swap.byidx( 1) end,
            { description = "swap with next client by index", group = "client" }),
  awful.key({ modkey, "Shift"            }, "k",   function() awful.client.swap.byidx(-1) end,
            { description = "swap with previous client by index", group = "client" }),
  awful.key({ modkey, "Control"          }, "j",   function() awful.tag.incmwfact(-0.05) end,
            { description = "increase master width factor", group = "layout"}),
  awful.key({ modkey, "Control"          }, "k",   function() awful.tag.incmwfact( 0.05) end,
            { description = "decrease master width factor", group = "layout" }),
  awful.key({ modkey, "Control", "Shift" }, "j",   function() awful.tag.incnmaster(-1, nil, true) end,
            { description = "decrease the number of master clients", group = "layout" }),
  awful.key({ modkey, "Control", "Shift" }, "k",   function() awful.tag.incnmaster( 1, nil, true) end,
            { description = "increase the number of master clients", group = "layout" }),
  awful.key({ modkey                     }, "Tab", function() awful.layout.inc( 1) end,
            { description = "select next", group = "layout" }),
  awful.key({ modkey, "Shift"            }, "Tab", function() awful.layout.inc(-1) end,
            { description = "select previous", group = "layout" }),
  awful.key({ modkey, "Shift"            }, "n",
            function()
                local c = awful.client.restore()
                -- Focus restored client
                if c then
                  c:activate { raise = true, context = "key.unminimize" }
                end
            end,
            { description = "restore minimized", group = "client" }),
  awful.key {
    modifiers   = { "Mod1" },
    keygroup    = "numrow",
    description = "select layout directly",
    group       = "layout",
    on_press    = function(index)
      local t = awful.screen.focused().selected_tag
      if t then
        t.layout = t.layouts[index] or t.layout
      end
    end,
  }
}

-- }}}
-- Inter-Tag {{{

awful.keyboard.append_global_keybindings{
  awful.key({ modkey                    }, "l",      awful.tag.viewnext,
            { description = "view next", group = "tag" }),
  awful.key({ modkey                     }, "h",      awful.tag.viewprev,
            {description = "view previous", group = "tag"}),
  awful.key({ modkey, "Shift"            }, "l",      function() awful.screen.focus_relative( 1) end,
            { description = "focus the next screen", group = "screen" }),
  awful.key({ modkey, "Shift"            }, "h",      function() awful.screen.focus_relative(-1) end,
            { description = "focus the previous screen", group = "screen" }),
  awful.key({ modkey                     }, "Escape", awful.tag.history.restore,
            { description = "go back", group = "tag" }),
  awful.key{
    modifiers   = { modkey },
    keygroup    = "numrow",
    description = "only view tag",
    group       = "tag",
    on_press    = function(index)
      local screen = awful.screen.focused()
      local tag = screen.tags[index]
      if tag then
        tag:view_only()
      end
    end,
  },
  awful.key {
    modifiers   = { modkey, "Control" },
    keygroup    = "numrow",
    description = "toggle tag",
    group       = "tag",
    on_press    = function(index)
      local screen = awful.screen.focused()
      local tag = screen.tags[index]
      if tag then
        awful.tag.viewtoggle(tag)
      end
    end,
  },
  awful.key {
    modifiers = { modkey, "Shift" },
    keygroup    = "numrow",
    description = "move focused client to tag",
    group       = "tag",
    on_press    = function(index)
      if client.focus then
        local tag = client.focus.screen.tags[index]
        if tag then
          client.focus:move_to_tag(tag)
        end
      end
    end,
  },
  awful.key {
    modifiers   = { modkey, "Control", "Shift" },
    keygroup    = "numrow",
    description = "toggle focused client on tag",
    group       = "tag",
    on_press    = function(index)
      if client.focus then
        local tag = client.focus.screen.tags[index]
        if tag then
          client.focus:toggle_tag(tag)
        end
      end
    end,
  },

}

-- }}}
-- Client {{{

client.connect_signal(
  "request::default_keybindings",
  function()
    awful.keyboard.append_client_keybindings{
      awful.key({ modkey, "Shift"   }, "c",      function(c) c:kill() end,
                { description = "close", group = "client" }),
      awful.key({ modkey,           }, "f",
                function(c)
                  c.fullscreen = not c.fullscreen
                  c:raise()
                end,
                { description = "toggle fullscreen", group = "client" }),
      awful.key({ modkey            }, "t",      function(c) c:swap(awful.client.getmaster()) end,
                { description = "move to master", group = "client" }),
      awful.key({ modkey,           }, "p",      function(c) c.ontop = not c.ontop end,
                { description = "toggle keep on top", group = "client" }),
      awful.key({ modkey, "Control" }, "space",  awful.client.floating.toggle,
                { description = "toggle floating", group = "client" }),
      awful.key({ modkey,           }, "n",      function(c) c.minimized = true end,
                { description = "minimize", group = "client" }),
      awful.key({ modkey,           }, "m",
                function(c)
                  c.maximized = not c.maximized
                  c:raise()
                end,
                { description = "toggle maximize", group = "client" }),
      awful.key({ modkey, "Shift", "Control" }, "l",      function(c) c:move_to_screen(c.screen.index + 1) end,
                { description = "move to next screen", group = "client" }),
      awful.key({ modkey, "Shift", "Control" }, "h",      function(c) c:move_to_screen(c.screen.index - 1) end,
                { description = "move to previous screen", group = "client" }),
    }
  end
)

-- }}}

-- }}}
-- {{{ Mouse bindings

-- Client {{{

client.connect_signal(
  "request::default_mousebindings",
  function()
    awful.mouse.append_client_mousebindings({
      awful.button({ }, 1, function(c)
        c:activate { context = "mouse_click" }
      end),
      awful.button({ modkey }, 1, function(c)
        c:activate { context = "mouse_click", action = "mouse_move"  }
      end),
      awful.button({ modkey }, 2, function(c) c:kill() end),
      awful.button({ modkey }, 3, function(c)
        c:activate { context = "mouse_click", action = "mouse_resize"}
      end),
    })
  end
)
-- }}}
-- Global {{{

awful.mouse.append_global_mousebindings{
    awful.button({ }, 3, function() mymainmenu:toggle() end),
    awful.button({ modkey }, 4, awful.tag.viewprev),
    awful.button({ modkey }, 5, awful.tag.viewnext),
}

-- }}}

-- }}}
