#┌────────────────────────────────────────────────────────────────────┐#
#│   _____                                     _____ __         ____  │#
#│  /__  /  ____  ____  ____ ___  ___  _____  / ___// /_  ___  / / /  │#
#│    / /  / __ \/ __ \/ __ `__ \/ _ \/ ___/  \__ \/ __ \/ _ \/ / /   │#
#│   / /__/ /_/ / /_/ / / / / / /  __/ /     ___/ / / / /  __/ / /    │#
#│  /____/\____/\____/_/ /_/ /_/\___/_/     /____/_/ /_/\___/_/_/     │#
#│                                                                    │#
#└────────────────────────────────────────────────────────────────────┘#
# ===== Init ========================================================= #

[ -d ${XDG_CACHE_HOME}/zsh ] || mkdir ${XDG_CACHE_HOME}/zsh
source ${HOME}/.zprofile

setopt autolist autoparamslash globcomplete extendedglob globdots markdirs histfindnodups \
       histsavenodups autocd SHARE_HISTORY INC_APPEND_HISTORY

# ===== Antigen ====================================================== #

source ${MOGA_CONFIGS}/zsh/.antigen
antigen bundle zsh-users/zsh-completions
antigen bundle zdharma-continuum/fast-syntax-highlighting
antigen apply

# ===== Prompt ======================================================= #

function set_prompt() {
    export PS1="$(prompt_func)"
}

function prompt_func() {
    echo "%{$reset_color%}$(__prompt_cmd1)$(__prompt_user_block)$(__prompt_git_block)"
    echo "%{$reset_color%}$(__prompt_cmd2)"
}

function __prompt_cmd1() {
    echo "%(?..%{$fg[red]%})┌─%{$reset_color%}"
}

function __prompt_cmd2() {
    echo "%(?..%{$fg[red]%})└─ "
}

function __prompt_user_block() {
    local usr_prompt="%{$fg[red]%}$USER%{$reset_color%}"
    echo "[$usr_prompt::$(__prompt_dir)]"
}

function __prompt_dir() {
    local cur_dir=$(echo ${PWD} | sed "s/\/home\/$USER/~/")

    # Replace root with git repo root
    local git_root=$(git rev-parse --show-toplevel 2> /dev/null)
    if [[ -n $git_root ]]; then
        local base=$(basename $git_root)
        local cur_dir=$(echo $PWD | sed "s/$(echo $git_root | sed 's_/_\\/_g')/$base/")
    fi

    local base=$(basename $cur_dir)
    local norm=$(echo $cur_dir | sed "s/${base}$//")
    local bold=$base

    local perm=$([ ! -w $PWD ] && echo "")
    echo "%{$fg[white]%}$norm%B$bold%b%{$fg[red]%}$perm%{$reset_color%}"
}

function __prompt_git_block() {
    local git_branch=$(git branch --show-current 2> /dev/null)
    [[ ! -n $git_branch ]] && return
    local git_branch="%{$fg[red]%}$git_branch%{$reset_color%}"

    echo "[$git_branch$(__prompt_git_status)]"
}

function __prompt_git_status() {
    local git_status="$(git status --porcelain --branch --untracked-files --rename)"

    local ahead=$(echo $git_status | rg --only-matching "ahead ([0-9]+)" -r '↑$1' )
    local bhind=$(echo $git_status | rg --only-matching "behind ([0-9]+)" -r '↓$1')

    local out="%{$fg[white]%}$ahead$bhind%{$reset_color%}"

    local sta_count=$(echo $git_status | rg --count-matches "^[ADM].")
    local staged=$([[ -n $sta_count ]] && echo "%{$fg[green]%}${sta_count}s")

    local ren_count=$(echo $git_status | rg --count-matches "^R ")
    local renamd=$([[ -n $ren_count ]] && echo "%{$fg[cyan]%}${ren_count}r")

    local tmp="${staged}${renamd}"
    local out=$([[ -n "$tmp" ]] && echo "$out|$tmp%{$reset_color%}" || echo "$out")

    local del_count=$(echo $git_status | rg --count-matches "^ D"  )
    local deletd=$([[ -n $del_count ]] && echo "%{$fg[red]%}${del_count}d")

    local mod_count=$(echo $git_status | rg --count-matches "^.M"  )
    local modifd=$([[ -n $mod_count ]] && echo "%{$fg[blue]%}${mod_count}m")

    local unt_count=$(echo $git_status | rg --count-matches "^\?\?")
    local untrkd=$([[ -n $unt_count ]] && echo "%{$fg[white]%}${unt_count}?")

    local tmp="${deletd}${modifd}${untrkd}"
    local out=$([[ -n $tmp ]] && echo "$out|$tmp%{$reset_color%}" || echo "$out")

    local stash_count=$(git stash list | wc -l)
    local stash=$([[ $stash_count -gt 0 ]] && echo "%{$fg[magenta]%} $stash_count%{$reset_color%}")
    local out=$([[ -n $stash ]] && echo "$out|$stash" || echo "$out")

    echo "$out"
}

# Prompt config
autoload -Uz colors && colors
precmd_functions+=(set_prompt)

# ===== Completion =================================================== #

autoload -U +X bashcompinit && bashcompinit
autoload -Uz compinit
zmodload zsh/complist

# Use cache
zstyle ':completion:*' use-cache yes
zstyle ':completion:*' cache-path $ZSH_CACHE_DIR

# Don't complete uninteresting users
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm amanda apache at avahi avahi-autoipd beaglidx bin cacti canna \
        clamav daemon dbus distcache dnsmasq dovecot fax ftp games gdm \
        gkrellmd gopher hacluster haldaemon halt hsqldb ident junkbust kdm \
        ldap lp mail mailman mailnull man messagebus  mldonkey mysql nagios \
        named netdump news nfsnobody nobody nscd ntp nut nx obsrun openvpn \
        operator pcap polkitd postfix postgres privoxy pulse pvm quagga radvd \
        rpc rpcuser rpm rtkit scard shutdown squid sshd statd svn sync tftp \
        usbmux uucp vcsa wwwrun xfs '_*'

# Menu select
zstyle ':completion:*' menu select

# Hyphen-insensitive
zstyle ':completion:*' matcher-list \
       'm:{a-zA-Z-_}={A-Za-z_-}' 'r:|=*' 'l:|=* r:|=*'

# Partial completion
zstyle ':completion:*' list-suffixes
zstyle ':completion:*' expand prefix suffix 

# ===== General settings ============================================= #

# asdf
source ${MOGA_CONFIGS}/zsh/.asdf/asdf.sh

export KEYTIMEOUT=1

HISTSIZE=1000
SAVEHIST=1000
HISTFILE=${XDG_CACHE_HOME}/zsh/history

# ===== Aliases ====================================================== #

alias cf='. cf'
alias lg='lazygit'
alias ll='eza -al'
alias la='eza -a'
alias ls='eza'

alias bb='zig build'
alias br='zig build run'
alias bt='zig build test'

# nvim
alias wiki='nvim -c VimwikiIndex'
alias gwiki='neovide -- -c VimwikiIndex'
alias nvimconf='nvim ~/.config/nvim/'
alias mogaconf='nvim ${MOGA}/configs/'
alias neovide='neovide --fork'

# tmux
alias tmux='tmux -f ~/.config/tmux/tmux.conf'

# ranger
alias cr='cd_ranger'
alias cd_ranger='ranger --choosedir=/tmp/rangerdir; LASTDIR=`cat /tmp/rangerdir`; cd "$LASTDIR"'

# git
alias gf='git fetch -p'
alias gp='git push'
alias gpf='git push -f'
alias gpl='git pull'
alias gplf='git pull -f'
alias grom='git rebase origin/master'

# get shit done
alias gsd='sudo gsd'

# ===== Binds ======================================================== #

# menu select
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

# history
bindkey '^R' history-incremental-pattern-search-backward
bindkey '^[[A' history-beginning-search-backward
bindkey '^[[B' history-beginning-search-forward

# ===== MOTD ========================================================= #

echo '( ._.)'
