local wezterm = require("wezterm")
local act = wezterm.action

local config = {}

config.front_end = "WebGpu"

config.max_fps = 240
config.enable_tab_bar = false
config.enable_scroll_bar = true
config.bold_brightens_ansi_colors = false

config.window_padding = { left = 0, right = 0, top = 0, bottom = 0 }

config.font_size = 11
config.font = wezterm.font_with_fallback {
  -- { family = "ac437 ibm vga 8x16", weight = "Regular", stretch="Normal", style="Normal" },
  -- { family = "fira code", weight = "Regular", stretch="Normal", style="Normal" },
  { family = "iosevkatermslab nerd font", weight = "Regular", stretch="Normal", style="Normal" },
  -- {
  --   family = "Monaspace Argon", weight = "Regular", stretch = "Normal", style = "Normal",
  --   harfbuzz_features = { 'ss01=0', 'ss02=1', 'ss03=1', 'ss04=1', 'ss05=1', 'ss06=0', 'ss06=0', 'ss08=1', 'ss09=0', 'calt=1' },
  -- },
  { family = "Inconsolata Nerd Font Mono", weight = "Regular", stretch="Normal", style="Normal" },
}

config.freetype_load_target = "Normal"
config.freetype_render_target = "Normal"
config.underline_thickness = "1.0pt"

config.colors = {
  foreground = "#D1B896",
  background = "#00161D",

  cursor_bg = "#D1B896",
  cursor_fg = "#00161D",
  cursor_border = "#D1B896",

  selection_fg = "#D1B896",
  selection_bg = "#273747",

  -- The color of the scrollbar "thumb"; the portion that represents the current viewport
  scrollbar_thumb = "#d1b896",

  ansi = {
    "#131415",
    "#C03B3B",
    "#2EC09C",
    "#E7C547",
    "#7AA6FF",
    "#D4BFFF",
    "#7AD0C6",
    "#E8DCCB",
  },

  compose_cursor = "#7AA6FF",

  copy_mode_active_highlight_bg = { Color = "#000000" },
  copy_mode_active_highlight_fg = { AnsiColor = "Black" },
  copy_mode_inactive_highlight_bg = { Color = "#52AD70" },
  copy_mode_inactive_highlight_fg = { AnsiColor = "White" },

  quick_select_label_bg = { Color = "peru" },
  quick_select_label_fg = { Color = "#FFFFFF" },
  quick_select_match_bg = { AnsiColor = "Navy" },
  quick_select_match_fg = { Color = "#FFFFFF" },
}

config.window_background_opacity = 0.95
config.disable_default_key_bindings = true

config.keys = {
  { key = 'Backspace', mods = 'SHIFT', action = act.SendKey { key = 'Backspace', mods = '' }, },

  { key = 'C', mods = 'CTRL|SHIFT', action = act.CopyTo('ClipboardAndPrimarySelection') },
  { key = 'V', mods = 'CTRL|SHIFT', action = act.PasteFrom('Clipboard')                 },

  { key = 'u', mods = 'ALT', action = act.ScrollByPage(-1) },
  { key = 'd', mods = 'ALT', action = act.ScrollByPage(1)  },
  { key = 'k', mods = 'ALT', action = act.ScrollByLine(-1) },
  { key = 'j', mods = 'ALT', action = act.ScrollByLine(1)  },
  { key = 'g', mods = 'ALT', action = act.ScrollToTop      },
  { key = 'G', mods = 'ALT', action = act.ScrollToBottom   },
  { key = 'L', mods = 'CTRL|ALT', action = act.ClearScrollback('ScrollbackAndViewport') },

  { key = 's', mods = 'ALT', action = act.QuickSelect      },
  { key = 'v', mods = 'ALT', action = act.ActivateCopyMode },
  { key = '/', mods = 'ALT', action = act.Search{ CaseInSensitiveString = "" } },

  { key = '0', mods = 'CTRL', action = act.ResetFontSize    },
  { key = '-', mods = 'CTRL', action = act.DecreaseFontSize },
  { key = '=', mods = 'CTRL', action = act.IncreaseFontSize },

  { key = 'Return', mods = 'ALT', action = act.SpawnWindow },
}

return config
