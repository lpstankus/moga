#!/bin/zsh

# Default programs
export BROWSER="firefox"
export EDITOR="nvim"
export READER="evince"
export TERMINAL="wezterm"
export LAUNCHER="rofi -show drun" # probably only using this on x11, so keep rofi instead of wofi

# XDG home dirs
export XDG_CACHE_HOME="${HOME}/.cache"
export XDG_CONFIG_HOME="${HOME}/.config"
export XDG_DATA_HOME="${HOME}/.local/share"
export XDG_RUNTIME_HOME="$XDG_CACHE_HOME}/runtime"

# Moga
export MOGA="${HOME}/Moga"
export MOGA_CONFIGS="${MOGA}/configs"
export MOGA_SCRIPTS="${MOGA}/scripts"
export MOGA_SOURCES="${MOGA}/sources"
export MOGA_THEMES="${MOGA}/themes"
export MOGA_XCONFIGS="${MOGA}/xconfigs"

# Cache paths
export BUNDLE_USER_CACHE="${XDG_CACHE_HOME}/bundle"
export GEM_SPEC_CACHE="${XDG_CACHE_HOME}/gem"
export SOLARGRAPH_CACHE="${XDG_CACHE_HOME}/solargraph"
export ZSH_COMPDUMP="${XDG_CACHE_HOME}/zsh/zcompdump-${HOST}"

# Config paths
export ANSIBLE_CONFIG="${XDG_CONFIG_HOME}/ansible/ansible.cfg"
export BUNDLE_USER_CONFIG="${XDG_CONFIG_HOME}/bundle"
export GTK2_RC_FILES="${XDG_CONFIG_HOME}/gtk-2.0/gtkrc-2.0"
export NPM_CONFIG_USERCONFIG="${XDG_CONFIG_HOME}/npm/npmrc"
export NVM_DIR="${XDG_CONFIG_HOME}"
export WGETRC="${XDG_CONFIG_HOME}/wget/wgetrc"

# Data paths
export ADOTDIR="${XDG_DATA_HOME}/antigen"
export ASDF_DATA_DIR="${XDG_DATA_HOME}/asdf"
export BUNDLE_USER_PLUGIN="${XDG_DATA_HOME}/bundle"
export CARGO_HOME="${XDG_DATA_HOME}/cargo"
export GEM_HOME="${XDG_DATA_HOME}/gem"
export GNUPGHOME="${XDG_DATA_HOME}/gnupg"
export GOPATH="${XDG_DATA_HOME}/go"
export JULIA_DEPOT_PATH="${XDG_DATA_HOME}/julia"
export PGADMIN_DATA="${XDG_DATA_HOME}/pgadmin"
export STACK_ROOT="${XDG_DATA_HOME}/stack"
export WEECHAT_HOME="${XDG_DATA_HOME}/weechat"
export rvm_path="${XDG_DATA_HOME}/rvm"

# Runtime paths
export LESSHISTFILE="${XDG_RUNTIME_DIR}"
export TMUX_TMPDIR="${XDG_RUNTIME_DIR}"

# ETC
export ANTIGEN_LOG="${ADOTDIR}/antigen.log"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"

# Postgress / pgadmin
export LOG_FILE="${PGADMIN_DATA}/pgadmin4.log"
export PGDATA="/var/lib/pgsql/11/data"
export SESSION_DB_PATH="${PGADMIN_DATA}/sessions"
export SQLITE="${PGADMIN_DATA}/pgadmin.db"
export STORAGE="${PGADMIN_DATA}/storage"

# Vulkan SDK config
VULKAN_SDK_PATH="$HOME/.local/libs/vulkan"
source "${VULKAN_SDK_PATH}/setup-env.sh"

# PATH setup
export PATH="${HOME}/.local/bin:${PATH}:/sbin:/opt/cuda/bin:${MOGA_SCRIPTS}"
export PATH="${PATH}:${GOPATH}/bin:${XDG_DATA_HOME}/gem/bin:${rvm_path}/bin:${CARGO_HOME}/bin"
