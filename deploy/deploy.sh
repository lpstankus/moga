#!/bin/sh

createLinks() {
    # Links for dirs MOGA_CONFIGS
    moga_configs=$(ls ${MOGA_CONFIGS})
    for config_file in ${moga_configs}; do
        config_path="${MOGA_CONFIGS:?}/${config_file}"
        rm -rf "${XDG_CONFIG_HOME:?}/${config_file}"
        ln -s "${config_path}" "${XDG_CONFIG_HOME}"
    done

    # Links for .profile and .zprofile
    rm -rf "${HOME}/.profile"
    rm -rf "${HOME}/.zprofile"
    ln -s "${HOME}/.zprofile" "${HOME}/.profile"
    ln -s "${DEPLOY_DIR}/profile.zsh" "${HOME}/.zprofile"
    ln -s "${MOGA_CONFIGS}/zsh/zshrc" "${HOME}/.zshrc"

    # Link for .zshrc
    rm -rf "${MOGA_CONFIGS}/zsh/.zshrc"
    ln -s "${MOGA_CONFIGS}/zsh/zshrc" "${MOGA_CONFIGS}/zsh/.zshrc"

    # Link for mouse and touchpad system configs (needs root permission)
    xconfigs=$(ls "${MOGA_XCONFIGS}")
    for file in ${xconfigs}; do
        sudo rm "/usr/share/X11/xorg.conf.d/${file}"
        sudo ln -s "${MOGA_XCONFIGS}/${file}" "/usr/share/X11/xorg.conf.d/${file}"
    done

    clear
}

handleDependencies() {
    # Shortcut for error messages
    dependencyError() { echo "ERROR!"; echo "Failed to handle $1"; exit 1; }

    # Antigen
    {
        rm -rf "${MOGA_CONFIGS}/zsh/.antigen"
        curl -L git.io/antigen > "${MOGA_CONFIGS}/zsh/.antigen"
    } || { dependencyError "Antigen"; }
    clear

    # asdf
    {
        rm -rf "${MOGA_CONFIGS}/zsh/.asdf"
        git clone https://github.com/asdf-vm/asdf.git ${MOGA_CONFIGS}/zsh/.asdf --branch v0.9.0
    } || { dependencyError "asdf"; }
}


DEPLOY_DIR="$({ cd "$(dirname "$0")" || exit 1; pwd; })"
. "${DEPLOY_DIR}/profile.zsh"

createLinks || { echo "Failed to create links"; exit 1; }
handleDependencies || exit 1

clear
